# Testing asynchronous code in Angular applications

1. Application intro
    - components
    - behaviour
2. Synchronous testing
    - sunny day scenario
    - missed bug scenario
3. Asynchronous testing
    - explanation of tools 
    - catching the bug
    - bad practices
 
* _stop karma service after each run_

## Application intro

[//]: <> (Show the page)
A simple application which shows user name info on a page. It has a service to retrieve a user name and a component to display it.
To get the information an async call is to be performed by the service. While the component is waiting for the response, the spinner
is to be displayed. (show )

[//]: <> (Show the code)

## Synchronous testing

Usually such a kind of functionality is tested via sync tests. Those are good for some cases but far from ideal. 

[//]: <> (Show the sync test)
This test works and even can catch some bugs for us.

[//]: <> (Run the sync test - should succeed)
[//]: <> (Change a spinner flag values and run the test  - should fail)

But the issue here is that we do not test here the async behaviour of the app.

[//]: <> (Move a spinner flag assignment to the init method and run the test - should succeed)
[//]: <> (Show the app behaviour - the spinner should not be displayed)

## Asynchronous testing

To avoid situations like this, we need to test the async behaviour of the app. For that angular has a special set of tools like
`waitForAsync` and `whenStable` functions. The former wraps our test function in a special async test zone. 
The test will automatically complete when all asynchronous calls within this zone are done. The latter is used to resume testing 
after events have triggered async activity or async change detection.

[//]: <> (Run the async test - should fail)
[//]: <> (Fix the code and run the test again- should succeed)
[//]: <> (Show both logs are displayed)

If we try to write our test without those tools, it won't work due to the fact that the second assertion will not wait the 
async service call to be returned and check the initial spinner flag value.

[//]: <> (Run the not working test - should fail)
[//]: <> (Show only one log is displayed)

We could try to write our test without using the `waitForAsync` and just with `whenStable`, but this even worse because the 
second assertion will be simply "omitted" by the test.

[//]: <> (Run the bad test - should succeed)
[//]: <> (Comment out the spinner flag assignment and run the test - should succeed)
[//]: <> (Show only one log is displayed)
[//]: <> (Show Karma server logs - should throw an error)

This happens because `whenStable` function is not working properly when it is used without the `waitForAsync`. The assertion in
the promise callback will not be performed as the test finishes its execution before the promise gets resolved.
