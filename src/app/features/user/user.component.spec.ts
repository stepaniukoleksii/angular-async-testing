import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserComponent } from './user.component';
import { UserService } from '../../services/user.service';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import SpyObj = jasmine.SpyObj;

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;
  let userServiceSpy: SpyObj<UserService>;

  beforeEach(waitForAsync(() => {
    userServiceSpy = jasmine.createSpyObj('UserService', ['getUserName']);

    TestBed.configureTestingModule({
      declarations: [UserComponent],
      providers: [{provide: UserService, useValue: userServiceSpy}]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
  });

  it('should display spinner (sync test)', () => {
    userServiceSpy.getUserName.and
      .returnValue(of('Test Name'));
    expect(component.showSpinner).toBeTrue();

    fixture.detectChanges();
    expect(component.showSpinner).toBeFalse();
  });

  it('should display spinner (async test)', waitForAsync(() => {
    userServiceSpy.getUserName.and
      .returnValue(of('Test Name').pipe(delay(1000)));

    fixture.detectChanges();
    expect(component.showSpinner).toBeTrue();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.showSpinner).toBeFalse();
    });
  }));

  it('should display spinner (not working test)', () => {
    userServiceSpy.getUserName.and
      .returnValue(of('Test Name').pipe(delay(1000)));

    fixture.detectChanges();
    expect(component.showSpinner).toBeTrue();

    fixture.detectChanges();
    expect(component.showSpinner).toBeFalse();
  });

  it('should display spinner (bad test)', () => {
    userServiceSpy.getUserName.and
      .returnValue(of('Test Name').pipe(delay(1000)));

    fixture.detectChanges();
    expect(component.showSpinner).toBeTrue();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.showSpinner).toBeFalse();
    });
  });
});
