import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userName = '';
  showSpinner = true;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUserName();
  }

  private getUserName(): void {
    console.log('...retrieving user name...');

    this.userService.getUserName()
      .subscribe(name => {
        this.userName = name;
        this.showSpinner = false;

        console.log('...user info is retrieved...');
      });
  }
}
