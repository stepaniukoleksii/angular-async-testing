import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  getUserName(): Observable<string> {
    return of('John Doe').pipe(
      delay(5000)
    );
  }
}
